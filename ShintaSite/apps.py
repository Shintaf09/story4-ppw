from django.apps import AppConfig


class ShintasiteConfig(AppConfig):
    name = 'ShintaSite'
