from django.shortcuts import render

# Create your views here.
def index(request):
	return render(request,"index.html")

def about(request):
	return render(request,"aboutme.html")

def resume(request):
	return render(request,"resume.html")

def contact(request):
	return render(request,"contact.html")

def gallery(request):
	return render(request,"gallery.html")
